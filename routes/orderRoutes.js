const express = require("express");
const router = express.Router(); 
const orderController = require("../controllers/orderController.js");
const userController = require("../controllers/userController.js")
const productController = require("../controllers/productController.js");


router.post("/createorder", (req, res) => {
	const newData = {
		userId: req.body.userId,
		productId: req.body.productId, 	
		quantity: req.body.quantity
	}	
	orderController.addOrder(req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router;